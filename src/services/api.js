import React from 'react';
class Api {
    constructor(){
        this.storage = {};
        this.baseUrl = "https://2jdg5klzl0.execute-api.us-west-1.amazonaws.com/default/EmployeesChart-Api"
    }

    getEmployeeForManager(managerId){
        return new Promise((resolve,reject) =>{
            if (this.storage[managerId] !== undefined ){
                console.log("CACHE HIT");
                resolve(this.storage[managerId]);
            }
            else {
                fetch(this.baseUrl+"?manager="+managerId)
                .then( (response)=>response.json()).then( (data)=>{
                    this.storage[managerId]= data ; 

                    //for that 90's retro effect
                    setTimeout( 
                        ()=>resolve(data),
                        1000
                    );
                }).catch(reject);
            }
        });
    }
}

export const API = new Api();