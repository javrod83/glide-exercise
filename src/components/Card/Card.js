import React,{useState,useEffect} from 'react';
import './Card.css';
import Spinner from '../Spinner/Spinner';
import {API} from '../../services/api';
import { Transition } from 'react-transition-group';

function Card(props) {
    const {id,first,last,office,department} = props.employee;
    const [loading, setLoading] = useState(false);
    const [expanded, setExpanded] = useState(false);
    const [children, setChildren] = useState([]);
   
    const duration = 300; 

    const defaultStyle = {
        transition: `all ${duration}ms ease-in-out`,
        opacity: .1,
      }

      const transitionStyles = {
        entering: { 
             opacity: 1,
            maxWidth:"100rem",
            // height:"2rem"
        },
        entered:  {
              opacity: 1,
              maxWidth:"100rem",
              //  height:"2rem"
            },
        exiting:  {
              opacity: .5,
              maxWidth:"4rem",
              //  height:0
            },
        exited:  { 
            opacity: 0.5,
            maxWidth:"4rem",
            
            // height:0
        }
      };

      const childrenStyle = {
        transition: `all ${duration/2}ms ease-in-out`,
        opacity: .1,
        overflow:"hidden",
       
      }

      const childrenStyles = {
        entering: { 
            opacity: 1,
            maxWidth:"100rem"
        },
        entered:  {
            opacity: 1,
            maxWidth:"100rem"
            },
        exiting:  {
            opacity: 0,
            maxWidth:"0"
            },
        exited:  { 
            opacity: 0,
            maxWidth:"0"
        }
      };
      
    useEffect(()=>{
        if (props.deepnes == 0 && !expanded)
        { expand();}
    })

    const expand = ()=>{
        setExpanded(true);
        setLoading(true);
        API.getEmployeeForManager(id)
        .then((data)=>{
            setChildren(data);
            setLoading(false);
        })
        .catch((err)=>console.log(err));
    }

    const collapse = () =>{
        setExpanded(false);
    }

    return (  <Transition in={expanded} timeout={duration}>
        {transitionState =>(<div key ={id} className="card-root"  >
            <div className="content"   style={{...defaultStyle, ...transitionStyles[transitionState]}}>
                {props.deepnes== 0? null:<div className="connector"></div>}
                {props.deepnes!== 0 && expanded?<div className="top"></div>:null }
                {expanded? <div className="info">
                    <div className="avatar" onClick={()=>collapse()}> {loading?<Spinner/>:first[0] +" " +last[0]} </div>
                    <div className="title">{first || "-" }</div>
                    <div className="title">{last || "-"}</div>
                    <div className="secondary">ID: {id || "-"}   {office?"off: "+office :null} {department?"dept: "+department :null}</div>
                </div>  
                :<div className="proxy" onClick={()=>expand()}  >
                    {props.deepnes==0? null:<div className="connector"></div>}
                    <div className="background"></div>
                    <div className="initials">{first[0] +" " +last[0]}</div>
                    <div className="plus">+</div>
                </div>}
                {children.length==0?null:<div className="bottom"></div>}
                <div className="children" style={{...childrenStyle, ...childrenStyles[transitionState]}}>
                    {children.map( (child,key)=>
                        <Card key={key} employee={child} deepnes={props.deepnes+1}  />
                    )}
                </div>
            </div>
          
        </div>)}
    </Transition>)
}

export default Card;
