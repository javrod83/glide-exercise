import React,{useState,useEffect} from 'react';
import './Chart.css';

import {API} from '../../services/api';
import Card from '../Card/Card';

function Chart(props) {

    const [chairMan,setChairMan] = useState(null);

    useEffect( ()=>{
       if (chairMan == null) API.getEmployeeForManager(0).then( (data)=>{
            setChairMan(data[0])
        })
    });

  return (
    <div className="chart-root">
       { chairMan ? <Card employee = {chairMan} deepnes ={0}/>:null} 
    </div>
  )
}

export default Chart;