import React from 'react';
import logo from './logo.svg';
import './App.css';

import Chart from './components/Chart/Chart'

function App() {
  return (
    <div className="App">
      <header >
       <h1>Big Corp Chart</h1>
      </header>
       <Chart/>
    </div>
  );
}

export default App;
